#!/usr/bin/env python3

import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def reverse(input_str):
    """reverse
    naive string reverse creating a new string
    strings in python are immutable
    """
    a=len(input_str)-1
    reverse_str=""
    while a >= 0:
        reverse_str+=input_str[a]
        a = a-1
    return reverse_str

def reverse_with_array(input_str):
    """reverse_with_array
    create a byte array so you can modify it inline
    """
    a=len(input_str)-1
    b=0
    input_array=bytearray(input_str,encoding='utf8')
    while a > b:
        # do a swap
        c=input_array[a]
        input_array[a]=input_array[b]
        input_array[b]=c
        a = a-1
        b = b+1
    return input_array.decode('utf8')

def main(args):
    if len(sys.argv) < 2:
        eprint(f"Usage: {sys.argv[0]} [input string]")
        sys.exit(1)
    input_str=sys.argv[1]

    reverse_str=reverse(input_str)
    print(f"reverse({input_str})={reverse_str}")

    reverse_str=reverse_with_array(input_str)
    print(f"reverse_with_array({input_str})={reverse_str}")

if __name__ == "__main__":
    main(sys.argv)